# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

from scrapy import Field, Item


class NewsScraperItem(Item):
    # define the fields for your item here like:
    # name = Field()
    roof_line = Field()
    title = Field()
    lead = Field()
    body = Field()
    pub_date = Field()
    crawl_date = Field()
    tags = Field()
    authors = Field()
    url = Field()
    comments = Field()
    sections = Field()
    images = Field()
    source = Field()

'''
Created on 10.04.2019

@author: Lumuel
'''

import scrapy
import re
from ..items import NewsScraperItem
from scrapy.spiders import Rule
from scrapy.linkextractors import LinkExtractor
import logging
import datetime
import dateparser
import os
import hashlib

#scrapy crawl swraktuellcrawler -o out.json -t json 2> log.txt

class SwrAktuellCrawler(scrapy.Spider):
    name = "swraktuellcrawler"
    strBaseURL = "https://www.swr.de"    
    start_urls =['https://www.swr.de/swraktuell/baden-wuerttemberg/suedbaden/suedbaden-100.html']
     
    logger = logging.getLogger(__name__)

    custom_settings = {
        'DEFAULT_ITEM_CLASS': 'RegioScraper.items.RegioScraperItem',
        'ITEM_PIPELINES': {
            'newsscraper.pipelines.NewsScraperPipeLine': 1
            }
        }

    def start_requests(self):
        #get the execute location and set the location of the cache file
        execute_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.cache_location = f"{execute_dir}/{self.name}_cache" 

        #check if the cache file already exists
        if os.path.exists(self.cache_location):
            print("Cache File exists!")
            #if it exists save the existing hashes in a list
            with open(self.cache_location) as f:
                self.hashes = f.readlines()
            
            #get the first 32 characters (hash code) of a line 
            self.hashes = [x[:32] for x in self.hashes]
            
            
            print(self.hashes)
        else:
            print("Cache File not found. Creating...")
            self.hashes = []

        #scrap every url, in this case only one
        for url in self.start_urls:
            yield scrapy.Request(url, self.parse_news_page)

    
    def isNoRealArticle(self, hash):
        """
        Filters out URLs that aren't real articles
        """
        #no Rückschau
        if hash == "5d7a96e937c3817ae9efcf975c797ab4":
            return True
        #no Standort
        if hash == "71b8aa3bc0c732faf74cd8aa9bfe03a0":
            return True
        #no Wetter
        if hash == "605f4d8378208ef5d5aa396f86e1ab47":
            return True
        #no Verkehr
        if hash == "661a59da1553324dfab0b5b9bcb856fc":
            return True                
        #no Programm
        if hash == "0fdacb6050f00def80d8cc7905cf4870":
            return True  
        #no TVShow
        if hash == "84e3689406010a18ddb53534a07ff634":
            return True   

    def parse_news_page(self, response):        
        articleList = response.css('article h2.hgroup a::attr(href)').getall()

        for article in articleList:
            # get the hash of the article url
            hash = hashlib.md5(article.encode('utf-8')).hexdigest()
            # check if article was already archived
            if hash not in self.hashes:
                if self.isNoRealArticle(hash):
                    continue
                # if not archive the article
                with open(self.cache_location, 'a+') as f:
                    hash_line = f"{hash}\t{article}\n"
                    f.write(hash_line)
                self.hashes.append(hash)
                yield response.follow(article, self.parse_article)
       
    def parse_article(self, response):
        newsScraperItem = NewsScraperItem()

        newsScraperItem['roof_line'] = ''.join(response.css('h1.hgroup span.topline::text').getall())
        newsScraperItem['title'] = response.css('span[itemprop="headline"]::text').get()

        newsScraperItem['lead'] = response.css('p.lead::text').get()
        
        #Getting headers and paragraphs out of the article textbody
        #Joining them to one string
        body = response.xpath('//div[@class="bodytext"]/p/text() | //div[@class="bodytext"]/h2/text()').extract()
        articleBody = '\n'.join(body)
        newsScraperItem['body'] = articleBody

        #Gets a list of image links
        newsScraperItem['images'] = response.xpath('//div[@class="bodytext"]/figure/div[@class="image"]/link/@href').extract()

        newsScraperItem['crawl_date'] = datetime.datetime.now()
        newsScraperItem['tags'] = response.css('head meta[name="keywords"]::attr(content)').get()
        newsScraperItem['authors'] = response.css('span[itemprop="author"] meta[itemprop="name"]::attr(content)').get()
        newsScraperItem['url'] = response.css('head meta[property="og:url"]::attr(content)').get()
        date = ''.join(response.xpath('//dd[@class="meta-updated"]/time/@datetime').extract()) #2019-03-25T16:52:00 format
        newsScraperItem['pub_date'] = datetime.datetime.strptime(date, '%Y-%m-%dT%H:%M:%S')
        #Gets the category of the article

        #swr aktuell, swr, swr heimat und ähnliches
        #newsScraperItem['sections'] = response.css('head meta[name="author"]::attr(content)').get()
        
        #Categories as list
        newsScraperItem['sections'] = response.css('a[class="tagleiste-element"]::text').getall()

        yield newsScraperItem
    
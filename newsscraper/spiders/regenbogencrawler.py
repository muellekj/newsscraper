'''
Created on 15.09.2018

@author: rothenha
'''

import scrapy
import re
from ..items import NewsScraperItem
import logging
import locale
import datetime
import os
import hashlib

#scrapy crawl regenbogencrawler -o out.json -t json 2> log.txt

class Regenbogencrawler(scrapy.Spider):
    locale.setlocale(locale.LC_TIME, "de_DE.utf8")
    name = "regenbogencrawler"
    strBaseURL = "https://www.regenbogen.de"
    
    start_urls =['https://www.regenbogen.de/nachrichten/regional/suedbaden-schwarzwald']
     
    logger = logging.getLogger(__name__)

    custom_settings = {
        'DEFAULT_ITEM_CLASS': 'RegioScraper.items.RegioScraperItem',
        'ITEM_PIPELINES': {
            'newsscraper.pipelines.NewsScraperPipeLine': 1
            }
        }

    def start_requests(self):
        # get the execute location and set the location of the cache file
        execute_dir = os.path.realpath(os.path.join(os.getcwd(), os.path.dirname(__file__)))
        self.cache_location = f"{execute_dir}/{self.name}_cache" 

        # check if the cache file already exists
        if os.path.exists(self.cache_location):
            # if it exists save the existing hashes in a list
            with open(self.cache_location) as f:
                self.hashes = f.readlines()
            self.hashes = [x.rstrip('\t') for x in self.hashes]
        else:
            self.hashes = []
        
        # scrap every url, in this case only one
        for url in self.start_urls:
            yield scrapy.Request(url, self.parse_news_page)
        

    def parse_news_page(self, response):   
        # get all articles on the page    
        articleList = response.css('a.teaser-nachricht__caption-link.teaser__hover-link::attr(href)').getall() 

        for article in articleList:
            # get the hash of the article url
            hash = hashlib.md5(article.encode('utf-8')).hexdigest()
            # check if article was already archived
            if hash not in self.hashes:
                # if not archive the article
                with open(self.cache_location, 'a+') as f:
                    hash_line = f"{hash}\t{article}\n"
                    f.write(hash_line)
                self.hashes.append(hash)
                yield response.follow(article, self.parse_article)
                
        # get the url of the next site
        lstNextURL = response.css('li.pager-next a::attr(href)').get()
        # if there is another site, archive it aswell
        if len(lstNextURL) > 0:
            yield response.follow(lstNextURL, self.parse_news_page)
                
    def parse_article(self, response):
        # parse the article into a json format
        titles = response.css('div.field-item.even::text').getall()
        newsScraperItem = NewsScraperItem()
        if len(titles) > 0:
            newsScraperItem['roof_line'] = titles[0]
        if len(titles) > 1:
            newsScraperItem['title'] = titles[1]

        newsScraperItem['lead'] = response.css('p.article-nachricht__introduction-text::text').get()

        paragraphs = response.css('div.article-nachricht__content.drupal__article-content p::text').getall()

        newsScraperItem['body'] = "\n\n".join(paragraphs)

        date = response.css('time.teaser-article__date::text').get()
        newsScraperItem['pub_date'] = datetime.datetime.strptime(date, 'am %d. %b %Y um %H:%M Uhr').strftime('%Y-%m-%d %H:%M:%S')
        newsScraperItem['crawl_date'] = datetime.datetime.now()
        newsScraperItem['tags'] = response.css('a.news__tags-link::text').getall()
        newsScraperItem['authors'] = response.css('span.teaser-article__author-link strong::text').get()
        newsScraperItem['url'] = response.request.url
        newsScraperItem['source'] = 'regenbogen'
        newsScraperItem['sections'] = []


        yield newsScraperItem
    